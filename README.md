Instructions on how to configure maven locally are available here: https://notes.inria.fr/lW6HzAZ_TbeN4KlrjnMNdw#

(Need to copy them here at some point)

To deploy packages to package registry:

- If it is a maven project, it is sufficient to follow the instructions in the pad and do a `mvn deploy`.

- If is a third-party jar that we want to store in our repo (like rapid-0.92.jar used by OntoSQL but not available anymore on maven repositories), we need to run the following:

    `curl --user "<username>:<token>" \
     --upload-file local-path-to-file \
    "https://gitlab.inria.fr/api/v4/projects/46547/packages/maven/  <path-to-package-and-file>"`

    For more details, check: https://docs.gitlab.com/ee/user/packages/generic_packages/#publishing-a-package-with-the-same-name-or-version
